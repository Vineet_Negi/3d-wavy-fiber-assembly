/*                      3D Worm-Like Chain Generator

This code generates a single 3D worm-like chain (as a sequence of points)
for given - 

N - number of links
x0,y0,z0 - starting point
nx0, ny0, nz0 - starting direction
alpha -  control parameter
*/



using namespace std;


//Utility functions
template <class T>
void cpy(T arr[], T arr_tar[], int N)
{
	// A templated function to copy to iterables
	for(auto i = 0; i < N; i++)
	{
		arr_tar[i] = arr[i];
	}
	return;
}

double norm(double arr[])
{
	// this function calculates the norm of a vector (3)
	double v = 0.0;
	for(auto i=0; i<3; i++)
	{
		v += pow(arr[i],2);
	}

	return sqrt(v);
}

// DEFINE CLASSES
class wlc
{
	public:
	int N;
	double le;
	double p0[3];
	double nx[3];
	double alpha;
	vector <double> x, y, z;
  
	/*This class define a single wlc chain */
	wlc(double step_length, double point[], double tangent[], double param){
		le = step_length;
		cpy <double> (point, p0, 3);
		cpy <double> (tangent, nx, 3);
		alpha = param;
	}

	void generate(double box_size)
	{
		// This function generates the wormlike chain (WLC) once the parameters have been set


		double t[3];
		double old_t[3];
		double n1[3];
		double n2[3];

		double temp_real;
		double phi, theta, r;
		
		int stop_flag = 0;
		int i=0;
		
		x.push_back(p0[0]);
		y.push_back(p0[1]);
		z.push_back(p0[2]);

		cpy<double>(nx, t, 3);

		// seed the random number generator
		random_device rd;
		mt19937 gen(rd());
		uniform_real_distribution <> uni_dis(0,1);
		
		while(stop_flag == 0)
		{	
			i++;
			
			//get the position of next point
			x.push_back(x[i-1] + le*t[0]);
			y.push_back(y[i-1] + le*t[1]);
			z.push_back(z[i-1] + le*t[2]);
					
			cpy<double>(t, old_t, 3);

			/*get new direction (here is the main algorithm)*/

			//calculate n1 (normal to t and (1,0,0) direction)
			n1[0] = 0;
			n1[1] = t[2];
			n1[2] = -t[1];

			temp_real = norm(n1);
			n1[0] /= temp_real; n1[1] /= temp_real; n1[2] /= temp_real;

			//calculate n2 (normal to t and n1 direction)
			n2[0] = -(t[1]*t[1] + t[2]*t[2]);
			n2[1] = t[0]*t[1];
			n2[2] = t[0]*t[2];

			temp_real = norm(n2);
			n2[0] /= temp_real; n2[1] /= temp_real; n2[2] /= temp_real;

			//get the angles phi (uniform between 0 and 2*pi) and theta
			r = uni_dis(gen);
			phi = 2*r*M_PI;

			r = uni_dis(gen);
			theta = sqrt(-(2/alpha)*log(1 - r));

			//calculate the new t vec
			t[0] = sin(theta)*cos(phi)*n1[0] + sin(theta)*sin(phi)*n2[0] + cos(theta)*old_t[0];
			t[1] = sin(theta)*cos(phi)*n1[1] + sin(theta)*sin(phi)*n2[1] + cos(theta)*old_t[1];
			t[2] = sin(theta)*cos(phi)*n1[2] + sin(theta)*sin(phi)*n2[2] + cos(theta)*old_t[2];
			
			//determine if the new point is outside the boundary
			if(x[i] > box_size/2 || x[i] < -box_size/2 || y[i] > box_size/2 || y[i] < -box_size/2)
			{
				stop_flag = 1;
				N = i;
			}
		}
	}
	
	~wlc()
	{	};

};



/*
int main(int argc, char *argv[])
{

	int N_steps = atoi(argv[1]);
	double l = atoi(argv[2]);
	double point[] = {0,0,0};
	double tangent[] = {0,1,0};
	double param = atoi(argv[3]);

	wlc c(l, N_steps, point, tangent, param);
	c.generate();

	write_model(c);

	return 1;
}
*/


  
