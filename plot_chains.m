% plot fibers
figure; 
axis equal
hold;

points = importdata('node_coords');
endnode_idx = importdata('endnode_idx');

N_chains = max(size(endnode_idx));
scale = 1;

start_idx = 1;
chain_lengths = zeros(N_chains,1);
mean_z = zeros(N_chains, 1);
slack = [];
for i = 1:N_chains
    end_idx = endnode_idx(i);
    
    chain_lengths(i) = end_idx - start_idx + 1;
    
    x = scale*points(start_idx:end_idx,1);
    y = scale*points(start_idx:end_idx,2);
    z = scale*points(start_idx:end_idx,3);
    
    if((max(y) > 0.51) | (min(y) < -0.51))
        i
        plot3(x,y,z, 'r');
    else
        plot3(x,y,z, 'b');
    end
    
    if(sum(x) ~= 0)
        path_length = get_length(x,y,z);
        l0 = sqrt((x(end) - x(1))^2 + (y(end) - y(1))^2);
        slack = [slack; (path_length - l0)/l0];
    end
    
    
    mean_z(i) = mean(z);
    

    
    start_idx = end_idx + 1;
end
    