This folder contains files (codes) to generate 3D fiber assemblies of tortuous fibers with given persistence length.
The fibers are generated in a box of size Lx Ly Lz. Lx X Ly rectangular domain in the XY plane defines the lateral boundaries of the fiber where as Lz defines the spread of the fibers. For more details on the model refer to 

https://doi.org/10.1039/D0SM01297A


