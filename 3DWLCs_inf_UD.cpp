/*                      3D Worm-Like Chain/fibers(s) Generator - Uniform Distribution

This code generates an assembly 3D worm-like chains (as a sequence of points)
for given simulation domain of size (Lx, Ly, Lz) centred around (Xc = 0 , Yc = 0, Zc = 0)

compile it as -

g++ -o executable_name 3DWLCs_inf_UD.cpp

and run as follows

./executable_name {Enter following parameters (in order) - Lx, Ly, Lz, N_fibers, element_length, persistense_length (in terms of number of elements), min_fiber_length (in terms of number of elements)}

Note: all chains have midpoints within the domain Lx, Ly, LZ
*/


#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <vector>
#include <random>
#include <math.h>

#include "3DWLC.h"

using namespace std;

// global vars to store the results
int N_chains;
double step_length, alpha;
double Lx, Ly, Lz;
double Xc, Yc, Zc;
vector <double*> node_coords;
vector <int> endnode_idx;
int min_chain_length;


// DEFINE FUNCTIONS
void write_model()
{
	// This function write the x,y,z data to a file - 'node_coords'
	ofstream outfile;
	
	//write node coords
	outfile.open("node_coords", ios::out);

	for(auto i=0; i < node_coords.size(); i++)
	{
		outfile << node_coords[i][0] << "\t" << node_coords[i][1] << "\t" << node_coords[i][2] << endl;
	}
	outfile.close();
	
	//write chain ends 
	outfile.open("endnode_idx", ios::out);

	for(auto i=0; i < endnode_idx.size(); i++)
	{
		outfile << endnode_idx[i] << endl;
	}
	outfile.close();
	
	return;

}

void get_chain(double point[], double tangent[], int chain_idx, double box_size, int min_chain_length, double z_offset)
{
	/*
	 * This function creates a single chain
	 */
	
	double center[3];
	double z_mean = 0.0;
		
	//create a chain object having a certain minimum chain length
	wlc c(step_length, point, tangent, alpha);
	c.generate(box_size);

	if(c.N > min_chain_length)
	{
		// setup the endnode_idx array
		int i0 = 0;
		
		if(endnode_idx.size() == 0)
		{
			endnode_idx.push_back(c.N + 1);
		}
		else
		{
			i0 = endnode_idx.back();
			endnode_idx.push_back(i0 + c.N + 1);
		}
		
		// get the mean z coord of the chain
		for(auto i =0; i<= c.N; i++)
		{
			z_mean += c.z[i];
		}
		z_mean = z_mean/c.N;
		
		//get the coords of the nodes and center them at 0
		for(auto i = 0; i <= c.N; i++)
		{
			node_coords.push_back((double*) calloc(3, sizeof(double)));
			node_coords[i + i0][0] = c.x[i];
			node_coords[i + i0][1] = c.y[i];
			node_coords[i + i0][2] = c.z[i] - z_mean + z_offset;
		}
	}
	
	return;
	
}


int main(int argc, char *argv[])
{

	if(argc == 8)
	{
		// read the fiber assembly generation data from the input stream
		Lx = atof(argv[1]);
		Ly = atof(argv[2]);
		Lz = atof(argv[3]);
		N_chains = atoi(argv[4]);
		step_length = atof(argv[5]);
		alpha = atof(argv[6]);
		min_chain_length = atoi(argv[7]);
	}
	else
	{
		cout<<"Error: not enough parameters. required - "<< 7 <<" entered - "<<(argc - 1)<<endl;;
		cout<<"Enter following parameters (in order) - Lx, Ly, Lz, N_fibers, element_length, persistense_length (in terms of number of elements), min_fiber_length (in terms of number of elements)"<<endl;
		exit(1);
		
	}
	
	//set the center point
	Xc = 0.0;
	Yc = 0.0;
	Zc = 0.0;
	
	//define z_offset para
	double z_offset;
		
	// define vars for chain generation
	int boundary_selection;
	double point[] = {0,0,0};
	double tangent[] = {1,0,0};
	
	//define misc vars
	double temp_real;
	
	// setup random device to generate randon integers
	random_device rd;
	mt19937 gen(rd());
	uniform_real_distribution <> uni_dis(0,1);
	normal_distribution <> norm_dist(0.0,1.0); // standard normal variate , mean = 0.0, var = 1.0
	
	//Perform the main loop and create the chains
	for(auto i = 0; i < N_chains; i++)
	{
		// randomly choose a boundary (1 - X, 2 - Y)
		boundary_selection =  (uni_dis(gen) < 0.5) ? (uni_dis(gen) < 0.5) ? -1 : 1 : (uni_dis(gen) < 0.5)  ? -2 : 2;
		
		switch (boundary_selection)
		{
			case -1: // (neg) X boundary
				point[0] = -Lx/2;
				point[1] = -Ly/2 + Ly*uni_dis(gen);
				point[2] = -Lz/2 + Lz*uni_dis(gen);
				break;
			case 1: // (pos) X boundary
				point[0] = Lx/2;
				point[1] = -Ly/2 + Ly*uni_dis(gen);
				point[2] = -Lz/2 + Lz*uni_dis(gen);
				break;
			case -2: //(neg) Y boundary
				point[0] = -Lx/2 + Lx*uni_dis(gen);
				point[1] = -Ly/2;
				point[2] = -Lz/2 + Lz*uni_dis(gen);
				break;
			case 2: //(pos) Y boundary
				point[0] = -Lx/2 + Lx*uni_dis(gen);
				point[1] = Ly/2;
				point[2] = -Lz/2 + Lz*uni_dis(gen);
				break;
		}
		
		//generate the tangent (based on point on unit a shpere)
		tangent[0] = norm_dist(gen); tangent[1] = norm_dist(gen); tangent[2] = norm_dist(gen);
		temp_real = norm(tangent);
		tangent[0] /= temp_real; tangent[1] /= temp_real; tangent[2] /= temp_real; 
		
		// the initial direction must be inward from a boundary
		switch (boundary_selection)
		{
			case -1: // (neg) X boundary
				tangent[0] = abs(tangent[0]);
				break;
			case 1: // (pos) X boundary
				tangent[0] = -1*abs(tangent[0]);
				break;
			case -2: //(neg) Y boundary
				tangent[1] = abs(tangent[1]);
				break;
			case 2: //(pos) Y boundary
				tangent[1] = -1*abs(tangent[1]);
				break;
		}
		
		// get a z_offset using a normal distribution
		z_offset = -Lz/2 + Lz*uni_dis(gen);
		
		//call the chain generator
		get_chain(point, tangent, i, Lx, min_chain_length, z_offset);				
	}
	
	write_model();
	
	// Free variables
	
	for(auto i = 0; i < node_coords.size(); i++)
	{
		free(node_coords[i]);
	}
		
	return 1;
}

